from git import Repo
import sys

GIT_REPO_FILEPATH='/Users/robertbelanec/Documents/websupport/test-repo/.git'

def git_push(commit_message):
    try:
        repo = Repo(GIT_REPO_FILEPATH)

        changed_files = [ item.a_path for item in repo.index.diff(None) ]

        if changed_files:
            repo.git.add(update=True)  
        
            repo.index.commit(commit_message)
            origin = repo.remote(name="origin")
            origin.push()
    except Exception as e:
        print("Some error occured while pushing the code: " + str(e))

git_push(str(sys.argv[1]))
